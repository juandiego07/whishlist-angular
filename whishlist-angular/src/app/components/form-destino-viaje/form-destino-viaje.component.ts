import { Component, EventEmitter, forwardRef, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { Inject } from '@angular/core';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';


@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud: number = 3;
  searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['', Validators.required]
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('Cambió el formulario: ', form);
    });
  }

  ngOnInit(): void {
    let elemNombre =  <HTMLInputElement>document.getElementById('nombre');
    // fromEvent(elemNombre, 'input')
    //   .pipe(
    //     map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
    //     filter(text => text.length > 4),
    //     // Se queda en stop éstas 2 decimas de segudno
    //     debounceTime(200),
    //     distinctUntilChanged(),
    //     switchMap(() => ajax('/assets/datos.json'))
    //   ).subscribe(AjaxResponse => {
    //     this.searchResults = AjaxResponse.response
    //   });

    fromEvent(elemNombre, 'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 4),
      // Se queda en stop éstas 2 decimas de segudno
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndPoint + '/ciudades?q=' + text))
    ).subscribe(AjaxResponse => this.searchResults = AjaxResponse.response);
  }

  guardar(nombre: string, url: string): boolean{
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl):{[s: string]: boolean}{
    const longitud = control.value.toString().trim().length;
    if (longitud>0 && longitud<4){
      return { invalidNombre: true};
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn{
    return (control: FormControl): {[s: string]: boolean } | null => {
      const longitud = control.value.toString().trim().length;
      if (longitud>0 && longitud<minLong){
        return { minLongNombre: true};
      }
      return null;
    }
  }

}
