import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ApiDestinoViaje } from '../../models/api-destino-viaje.model';
import { DestinoViaje } from '../../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ ApiDestinoViaje ]
})
export class ListaDestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  destinos: DestinoViaje[];
  destinosSugeridos: string[];
  updates: string[];
  all;
  
  constructor(public apiDestinoViaje: ApiDestinoViaje, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    // this.destinosSugeridos = ['Cali','Cartagena','San Andrés','Santa Marta'];
    this.updates = [];
    //Se incluye el Redux de la lección 3.4 de la Semana 2:
    this.store.select(state => state.destinos.favorito)
      .subscribe (d => {
        if (d != null) {
          this.updates.push('Se ha elegido a ' + d.nombre);
        }
      });
      store.select(state => state.destinos.items).subscribe(items => this.all = items); 
  }

  ngOnInit(): void {
  }

    /*Comentado en la lección 1.2 de la semana 2, se cambió por el método "agregado"
    guardar(nombre:string, url:string):boolean{
  	this.destinos.push(new DestinoViaje(nombre, url));
  	return false;
  }*/

  agregado(d: DestinoViaje){
    this.apiDestinoViaje.add(d);
    this.onItemAdded.emit(d);
    //Agregado en la lección 3.4 de la semana 2:
    //Eliminado en la lección 4.2 de la semana 2, porque ahora está en el ApiClient:
    //this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(e: DestinoViaje){
    //Comentado en la lección 2.1 de la semana 2
    //this.destinos.forEach(function (x) {x.setSelected(false); });
    //Comentado en la lección 3.1 de la semana 2
    /*this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    e.setSelected(true);*/
    this.apiDestinoViaje.elegir(e);
    //Agregado en la lección 3.4 de la semana 2:
    //Eliminado en la lección 4.2 de la semana 2, porque ahora está en el ApiClient:
    //this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  getAll(){

  }

}
