import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteDownAction, VoteResetAction, VoteUpAction } from '../../models/destino-viaje-state.model';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { trigger, state, style, transition, animate } from '@angular/animations'

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ]),
  ]
})

export class DestinoViajeComponent implements OnInit {

  @Output() onClicked: EventEmitter<DestinoViaje>;
  @Input() destino: DestinoViaje;
  @HostBinding('attr.class') ccsClass = 'col-md-4';
  @Input('idx') position: number;

  constructor(private store: Store<AppState>) { 
    this.onClicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
    this.onClicked.emit(this.destino);
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
  voteReset() {
    this.store.dispatch(new VoteResetAction(this.destino));
    return false;
  }

}
