import { Component, inject, InjectionToken, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { ApiDestinoViaje } from 'src/app/models/api-destino-viaje.model';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';

// class ApiDestinoViajeViejo {
//   getById(id: string): DestinoViaje {
//     console.log('Llamado por la clase vieja');
//     return null;
//   }
// }

// interface AppConfig {
//   apiEndPoint: string;
// };

// const APP_CONFIG_VALUE: AppConfig = {
//   apiEndPoint: 'mi_api.com'
// };

// const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

// class ApiDestinoViajeDecorated extends ApiDestinoViaje {

//   // constructor(@inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
//   //   super(store);
//   // }

//   getById(id: string): DestinoViaje {
//     console.log('Llamado por clase decorada!');
//     console.log('Config: ' + this.config.apiEndPoint);
//     // return super.getById(id);
//     return null;
//   }

// }

@Component({
  selector: 'app-detalle-destino',
  templateUrl: './detalle-destino.component.html',
  styleUrls: ['./detalle-destino.component.css'],
  providers: 
  [
    ApiDestinoViaje
    // { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    // { provide: ApiDestinoViaje, useClass: ApiDestinoViajeDecorated },
    // { provide: ApiDestinoViajeViejo, useExisting: ApiDestinoViaje }
  ]
})

export class DetalleDestinoComponent implements OnInit {

  // destino: DestinoViaje;

  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  }

  constructor(private route: ActivatedRoute, private apiDestinoViaje: ApiDestinoViaje) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    // this.destino = this.apiDestinoViaje.getById(id);
  }

}
