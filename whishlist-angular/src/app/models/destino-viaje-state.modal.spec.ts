import { reducerDestinosViajes, DestinoViajeState, initializeDestinosViajesState, InitMyDataAction, NuevoDestinoAction} from './destino-viaje-state.model';
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
    it( 'should reduce init data', () => {
        //Setup
        const prevState: DestinoViajeState = initializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 1']);
        //Action
        const newState: DestinoViajeState = reducerDestinosViajes(prevState, action);
        //Assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });

    it( 'should reduce new item added', () => {
        const prevState: DestinoViajeState = initializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Barcelona', 'url'));
        const newState: DestinoViajeState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('Barcelona');
    });
});