import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destino-viaje-state.model';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';


@Injectable()
export class ApiDestinoViaje {
	//Se eliminan estas líneas en la lección 4.2 de la semana 2:
	/*destinos: DestinoViaje[];
	current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);*/
	destinos: DestinoViaje[] = [];
	constructor(private store: Store<AppState>,
		@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, private http: HttpClient) {
		//Se elimina esta línea en la lección 4.2 de la semana 2:
		//this.destinos = [];
		this.store
			.select(state => state.destinos)
			.subscribe((data) => {
				console.log('Destinos sub store');
				console.log(data);
				this.destinos = data.items;
			});
		this.store
			.subscribe((data) => {
				console.log('All store');
				console.log(data);
			});
	}

	add(d: DestinoViaje) {
		const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
		const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', { nuevo: d.nombre }, { headers: headers });
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
			if (data.status === 200) {
				this.store.dispatch(new NuevoDestinoAction(d));
				const myDb = db;
				myDb.destinos.add(d);
				console.log('Todos los destinos de la DB!');
				myDb.destinos.toArray().then(destinos => console.log(destinos))
			}
		});
	}
	// add(d: DestinoViaje) {
	// 	//Se elimina esta línea en la lección 4.2 de la semana 2:
	// 	//this.destinos.push(d);
	// 	this.store.dispatch(new NuevoDestinoAction(d));
	// }
	//Se eliminó el getAll() en la lección 4.2 de la semana 2:
	/*getAll(): DestinoViaje[] {
	  return this.destinos;
	}
	
	/* BORRAR. LO TOMÉ DE UN VIDEO, PERO NO LO VEO UTILIZADO HASTA AHORA
	getById(id: String): DestinoViaje {
		return this.destinos.filter(function(d) {return d.id.toString() === id; })[0];
	}*/

	elegir(d: DestinoViaje) {
		////Se eliminan estas líneas en la lección 4.2 de la semana 2:
		/*this.destinos.forEach(x => x.setSelected(false));
		d.setSelected(true);
		this.current.next(d);*/
		this.store.dispatch(new ElegidoFavoritoAction(d));
	}
	//Se eliminó el subscribeOnChange(fn) en la lección 4.2 de la semana 2:
	/*subscribeOnChange(fn){
		this.current.subscribe(fn);
	}*/

} 